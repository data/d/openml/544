# OpenML dataset: transplant

https://www.openml.org/d/544

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

DATA FILE:
Data on patient deaths within 30 days of surgery in 131 U.S.
hospitals.  See Christiansen and Morris, Bayesian Biostatistics, D.
Berry and D. Stangl, editors, 1996, Marcel Dekker, Inc.


Data on 131 heart transplant hospitals in the US.  The 3646 transplants
took place during a 27 month period from October 1987 through December
1989.  The columns are:  obs = hospital #, e = expected #
of deaths within 30 days of the transplant surgeries, z = number of
deaths within 30 days of surgery, n = # of patients receiving heart
transplant within this time period.  (Christiansen and Morris, Bayesian
Biostatistics, D.  Berry and D. Stangl, editors, 1996.) The patient
level data used to create this data set was provided by the United
Network for Organ Sharing, 1100 Boulders Parkway, Suite 500, P.O. Box
13770, Richmond, VA, 23225.

The following data may be used for non-commercial purposes and can be
distributed freely.  If you use the data, please acknowledge StatLib,
the United Network for Organ Sharing, and Christiansen and Morris,
1996.




Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: none specific

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/544) of an [OpenML dataset](https://www.openml.org/d/544). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/544/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/544/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/544/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

